package com.buscape.teste.submarino;

import com.buscape.teste.submarino.Submarino;
import com.buscape.teste.submarino.comando.validador.ValidadorComando;
import com.buscape.teste.submarino.comando.validador.ValidadorException;
import com.buscape.teste.submarino.cordenada.calculo.FactoryCalculoCordenada;
import com.buscape.teste.submarino.cordenada.interpletador.InterpletarCordenada;
import com.buscape.teste.submarino.direcao.DeterminadorDirecao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("moverSubmarinoSimples")
public class MoverSubmarinoSimples implements  MoverSubmarino {


    private final Logger LOGGER = LogManager.getLogger(this.getClass());

    @Autowired
    @Qualifier("factoryCalculoCordenadaSimples")
    private FactoryCalculoCordenada factoryCalculoCordenada;

    @Autowired
    @Qualifier("interpletarCordenadaSimples")
    private InterpletarCordenada interpletarCordenada;

    @Autowired
    @Qualifier("determinadorDirecaoSimples")
    private DeterminadorDirecao determinadorDirecao;

    @Autowired
    private ValidadorComando validadorComando;

    @Override
    public Submarino mover(Submarino submarino){


        Submarino submarinoNovo = new Submarino(submarino.getComandos());


        try {
            validadorComando.validarEntrada(submarinoNovo.getComandos());
        } catch (ValidadorException e) {
            LOGGER.error(e.getMessage(),e);
            throw  new RuntimeException(e.getMessage(),e);
        }


        submarinoNovo.setCordenada(interpletarCordenada.interpletar(factoryCalculoCordenada,submarino.getComandos()));

        submarinoNovo.setDirecao(determinadorDirecao.calcularDirecao(submarinoNovo));


        return submarinoNovo;
    }
}
