package com.buscape.teste.submarino.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.buscape.teste.submarino")
public class SubmarinoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SubmarinoApplication.class, args);
    }
}
