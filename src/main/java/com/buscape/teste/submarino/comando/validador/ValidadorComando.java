package com.buscape.teste.submarino.comando.validador;


import com.buscape.teste.submarino.cordenada.ComandoEnum;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ValidadorComando {



    public void validarEntrada(String comando) throws  ValidadorException{

        if(StringUtils.isEmpty(comando) || comando.toUpperCase().endsWith(ComandoEnum.M.valor()) || !comando.matches("[a-zA-Z\\s]+")){

            throw  new ValidadorException("A entrada não é valida ela tem que ter os comando L=esqueda,D=direira,U=subir,D=descer,M=mover e não pode terminar com um comando M");
        }
    }


}
