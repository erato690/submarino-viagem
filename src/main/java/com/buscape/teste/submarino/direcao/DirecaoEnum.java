package com.buscape.teste.submarino.direcao;

public enum DirecaoEnum {

     NORTE("NORTE"),SUL("SUL"),LESTE("LESTE"),OESTE("OESTE");

     private String direcao;


     private DirecaoEnum(String direcao){

         this.direcao = direcao;
     }

    public String getDirecao() {
        return direcao;
    }
}
