package com.buscape.teste.submarino.direcao;

import com.buscape.teste.submarino.Submarino;
import com.buscape.teste.submarino.cordenada.Cordenada;

public interface DeterminadorDirecao {

    public String calcularDirecao(Submarino submarino);
}
