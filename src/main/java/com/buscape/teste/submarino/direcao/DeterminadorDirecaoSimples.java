package com.buscape.teste.submarino.direcao;

import com.buscape.teste.submarino.Submarino;
import com.buscape.teste.submarino.cordenada.Cordenada;
import org.springframework.stereotype.Component;

@Component("determinadorDirecaoSimples")
public class DeterminadorDirecaoSimples implements  DeterminadorDirecao {
    @Override
    public String calcularDirecao(Submarino submarino) {


        Cordenada cordenada = submarino.getCordenada();

        int eixoX =0;
        int eixoY =0;
        boolean valoreNegativo = false;

        if(cordenada.getEixoX() < 0){

            valoreNegativo = true;
            eixoX = cordenada.getEixoX() * -1;
        }

        if(cordenada.getEixoY() < 0  ) {

            valoreNegativo = true;
            eixoY = cordenada.getEixoY() * -1;
        }

        if(cordenada.getEixoY() > 0 && cordenada.getEixoY() > 0){

            eixoX = cordenada.getEixoX();
            eixoY = cordenada.getEixoY();
        }

        if(eixoX > eixoY && valoreNegativo)
            return DirecaoEnum.OESTE.getDirecao();
        else if(eixoX > eixoY && !valoreNegativo)
            return DirecaoEnum.LESTE.getDirecao();
        else if(eixoX < eixoY && valoreNegativo)
            return DirecaoEnum.SUL.getDirecao();
        else
            return DirecaoEnum.NORTE.getDirecao();

    }
}
