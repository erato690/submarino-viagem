package com.buscape.teste.submarino;

import com.buscape.teste.submarino.cordenada.Cordenada;

import java.io.Serializable;

public class Submarino implements Serializable {


    private Cordenada cordenada;
    private String direcao;

    private String comandos;

    private String mensage;


    public Submarino(String comandos){

        this.comandos = comandos;
    }

    public Cordenada getCordenada() {
        return cordenada;
    }

    public void setCordenada(Cordenada cordenada) {
        this.cordenada = cordenada;
    }

    public String getDirecao() {
        return direcao;
    }

    public void setDirecao(String direcao) {
        this.direcao = direcao;
    }

    public String getComandos() {
        return comandos;
    }

    public String getMensage() {
        return mensage;
    }

    public void setMensage(String mensage) {
        this.mensage = mensage;
    }
}
