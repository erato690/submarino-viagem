package com.buscape.teste.submarino.resource;

import com.buscape.teste.submarino.MoverSubmarino;
import com.buscape.teste.submarino.Submarino;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/submarino/simples")
public class SubmarinoResource {


    private final Logger LOGGER = LogManager.getLogger(this.getClass());


    @Autowired
    @Qualifier("moverSubmarinoSimples")
    private MoverSubmarino moverSubmarino;

    @RequestMapping(method = RequestMethod.GET,path = "/mover",params = "comando",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Submarino mover(@RequestParam("comando") String comando){

        Submarino submarino = new Submarino(comando);

        try {
            submarino = moverSubmarino.mover(submarino);
        }catch (Exception ex){

            LOGGER.error("Erro ao mover o submarino",ex);
            submarino.setMensage(ex.getMessage());

        }
        return submarino;
    }
}
