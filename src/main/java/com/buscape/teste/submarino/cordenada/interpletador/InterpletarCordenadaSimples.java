package com.buscape.teste.submarino.cordenada.interpletador;

import com.buscape.teste.submarino.cordenada.ComandoEnum;
import com.buscape.teste.submarino.cordenada.Cordenada;
import com.buscape.teste.submarino.cordenada.calculo.CalculoCordenada;
import com.buscape.teste.submarino.cordenada.calculo.FactoryCalculoCordenada;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Component("interpletarCordenadaSimples")
public class InterpletarCordenadaSimples implements  InterpletarCordenada {


    public Cordenada interpletar(FactoryCalculoCordenada factoryCalculoCordenada,String comandoCordenada){

        int eixoX =0;
        int eixoY =0;



        comandoCordenada = comandoCordenada.toUpperCase();

        String [] listaComandos = comandoCordenada.split(ComandoEnum.M.valor());

        listaComandos[listaComandos.length -1] = "V";


        for(int countlistaComando =0; countlistaComando < listaComandos.length ; countlistaComando++){

            String comandos = obterListasdecomando(listaComandos,countlistaComando);


            for(int countComando =0; countComando < comandos.length(); countComando++) {

                String comando = comandos.substring(countComando,countComando+1);

                CalculoCordenada calculoCordenada = factoryCalculoCordenada.FactoryCalculoCordenada(comando);
                Cordenada cordenada = calculoCordenada.calcular(comando);
                eixoX += cordenada.getEixoX();
                eixoY += cordenada.getEixoY();

            }

        }





        return new Cordenada(eixoX,eixoY);
    }

    private String obterListasdecomando(String [] listaComandos,int indexListaComando){

        if(StringUtils.isEmpty(listaComandos[indexListaComando]))
            return obterListasdecomando(listaComandos,indexListaComando-1);

        return listaComandos[indexListaComando];


    }



}
