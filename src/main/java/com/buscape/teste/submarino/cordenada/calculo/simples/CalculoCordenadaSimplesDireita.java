package com.buscape.teste.submarino.cordenada.calculo.simples;

import com.buscape.teste.submarino.cordenada.Cordenada;
import com.buscape.teste.submarino.cordenada.calculo.CalculoCordenada;
import org.springframework.stereotype.Component;

@Component("calculoCordenadaSimplesDireita")
public class CalculoCordenadaSimplesDireita implements CalculoCordenada {

    @Override
    public Cordenada calcular(String cordenada) {
        return new Cordenada(1,0);
    }
}
