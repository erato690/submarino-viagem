package com.buscape.teste.submarino.cordenada.interpletador;

import com.buscape.teste.submarino.cordenada.Cordenada;
import com.buscape.teste.submarino.cordenada.calculo.FactoryCalculoCordenada;

import java.util.List;

public interface InterpletarCordenada {


    public Cordenada interpletar(FactoryCalculoCordenada factoryCalculoCordenada, String comandoCordenada);
}
