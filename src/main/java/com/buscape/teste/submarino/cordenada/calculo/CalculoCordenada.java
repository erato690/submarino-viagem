package com.buscape.teste.submarino.cordenada.calculo;

import com.buscape.teste.submarino.cordenada.Cordenada;

public interface CalculoCordenada {

    public Cordenada calcular(String cordenada);


}

