package com.buscape.teste.submarino.cordenada.calculo.simples;

import com.buscape.teste.submarino.cordenada.ComandoEnum;
import com.buscape.teste.submarino.cordenada.calculo.CalculoCordenada;
import com.buscape.teste.submarino.cordenada.calculo.FactoryCalculoCordenada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("factoryCalculoCordenadaSimples")
public class FactoryCalculoCordenadaSimples implements FactoryCalculoCordenada {


    @Autowired
    @Qualifier("calculoCordenadaSimplesDireita")
    private CalculoCordenada calculoDireita;

    @Autowired
    @Qualifier("calculoCordenadaSimplesEsquerda")
    private CalculoCordenada calculoEsquerda;

    @Autowired
    @Qualifier("calculoCordenadaSimplesSubir")
    private CalculoCordenada calculoSubir;

    @Autowired
    @Qualifier("calculoCordenadaSimplesDescer")
    private CalculoCordenada calculoDescer;

    @Autowired
    @Qualifier("calculoCordenadaVazio")
    private CalculoCordenada calculoZerado;



    @Override
    public CalculoCordenada FactoryCalculoCordenada(String comando) {

        if(comando.equals(ComandoEnum.L.valor()))
            return calculoEsquerda;
        else if(comando.equals(ComandoEnum.R.valor()))
            return calculoDireita;
        else if(comando.equals(ComandoEnum.U.valor()))
            return calculoSubir;
        else if(comando.equals(ComandoEnum.D.valor()))
            return calculoDescer;
        else
            return calculoZerado;

    }
}
