package com.buscape.teste.submarino.cordenada.calculo;

import com.buscape.teste.submarino.cordenada.Cordenada;
import org.springframework.stereotype.Component;

@Component("calculoCordenadaVazio")
public class CalculoVazio implements CalculoCordenada {
    @Override
    public Cordenada calcular(String cordenada) {
        return new Cordenada(0,0);
    }
}
