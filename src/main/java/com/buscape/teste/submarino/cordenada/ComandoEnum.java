package com.buscape.teste.submarino.cordenada;

public enum ComandoEnum {



    L("L"),R("R"),M("M"),U("U"),D("D"),V("V");


    private String comando;

    private ComandoEnum(String comando){

        this.comando = comando;
    }

    public String valor(){

        return comando;
    }
}
