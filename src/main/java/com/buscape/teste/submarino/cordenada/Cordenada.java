package com.buscape.teste.submarino.cordenada;

public class Cordenada {

    private int eixoX;
    private int eixoY;



    public Cordenada (int eixoX , int eixoY){

        this.eixoX =eixoX;
        this.eixoY =eixoY;

    }

    public int getEixoX() {
        return eixoX;
    }

    public int getEixoY() {
        return eixoY;
    }


}
