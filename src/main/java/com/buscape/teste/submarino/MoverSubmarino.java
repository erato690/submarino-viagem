package com.buscape.teste.submarino;

public interface MoverSubmarino {

    public Submarino mover(Submarino submarino);
}
