package com.buscape.teste.submarino;

import com.buscape.teste.submarino.comando.validador.ValidadorComando;
import com.buscape.teste.submarino.comando.validador.ValidadorException;
import com.buscape.teste.submarino.config.SubmarinoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SubmarinoApplication.class})
public class ValidacaoComando {

    @Autowired
    private ValidadorComando validadorComando;

    private String comandoVazio="";
    private String comandoComNumero="asdasd2112";
    private String comandoValido="LDMUDD";

    private String comandoIvalidoterminadocomM="LDMUDDM";



    @Test(expected = ValidadorException.class)
    public void validarEntradaInvalidos() throws ValidadorException {

       validadorComando.validarEntrada(comandoVazio);

       validadorComando.validarEntrada(comandoComNumero);

       validadorComando.validarEntrada(comandoIvalidoterminadocomM);

    }

    @Test
    public void validarEntradaValidas() throws ValidadorException {

        validadorComando.validarEntrada(comandoValido);


    }

}
