package com.buscape.teste.submarino;


import com.buscape.teste.submarino.config.SubmarinoApplication;
import com.buscape.teste.submarino.cordenada.Cordenada;
import com.buscape.teste.submarino.cordenada.calculo.FactoryCalculoCordenada;
import com.buscape.teste.submarino.cordenada.interpletador.InterpletarCordenada;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SubmarinoApplication.class})
public class CalculoCordenadaSimpleTeste {


    @Autowired
    @Qualifier("interpletarCordenadaSimples")
    private InterpletarCordenada interpletarCordenada;

    @Autowired
    @Qualifier("factoryCalculoCordenadaSimples")
    private FactoryCalculoCordenada factoryCalculoCordenada;



    private String comandosSul;
    private String comandosNorte;
    private String comandosLeste;
    private String comandosOeste;


    @Before
    public void carregarVariavel(){

        comandosSul = "LMRDDMMUU";
        comandosNorte= "LMMUUUMDMUMD";
        comandosLeste="LLMRMDDMU";
        comandosOeste="RRRMMMD";
    }



    @Test
    public void  direcaoSul(){


        Cordenada cordenada = interpletarCordenada.interpletar(factoryCalculoCordenada,comandosSul);

        Assert.assertTrue(cordenada.getEixoX() == 1);

        Assert.assertTrue(cordenada.getEixoY() == -4);
    }


    @Test
    public void  direcaoLeste(){


        Cordenada cordenada = interpletarCordenada.interpletar(factoryCalculoCordenada,comandosLeste);


        System.out.println(cordenada.getEixoX());
        Assert.assertTrue(cordenada.getEixoX() == -1);

        Assert.assertTrue(cordenada.getEixoY() == -2);
    }

    @Test
    public void  direcaoOeste(){


        Cordenada cordenada = interpletarCordenada.interpletar(factoryCalculoCordenada,comandosOeste);


        System.out.println(cordenada.getEixoX());
        Assert.assertTrue(cordenada.getEixoX() == 9);

        Assert.assertTrue(cordenada.getEixoY() == 0);
    }


    @Test
    public void  direcaoNorte(){


        Cordenada cordenada = interpletarCordenada.interpletar(factoryCalculoCordenada,comandosNorte);


        System.out.println(cordenada.getEixoX());
        Assert.assertTrue(cordenada.getEixoX() == -2);

        Assert.assertTrue(cordenada.getEixoY() == 3);
    }



}
