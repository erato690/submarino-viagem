package com.buscape.teste.submarino;

import com.buscape.teste.submarino.config.SubmarinoApplication;
import com.buscape.teste.submarino.cordenada.Cordenada;
import com.buscape.teste.submarino.direcao.DeterminadorDirecao;
import com.buscape.teste.submarino.direcao.DirecaoEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SubmarinoApplication.class})
public class DeterminadorDirecaoSimplesTest {

    @Autowired
    @Qualifier("determinadorDirecaoSimples")
    private DeterminadorDirecao determinadorDirecao;

    private Submarino submarino;



    @Test
    public void direcaoNorte(){

        submarino = new Submarino("LDTGDD");
        submarino.setCordenada( new Cordenada(0,5));


        Assert.assertTrue(determinadorDirecao.calcularDirecao(submarino).equals(DirecaoEnum.NORTE.getDirecao()));
    }

    @Test
    public void direcaoSul(){

        submarino = new Submarino("LDTGDD");
        submarino.setCordenada( new Cordenada(-2,-5));


        Assert.assertTrue(determinadorDirecao.calcularDirecao(submarino).equals(DirecaoEnum.SUL.getDirecao()));
    }

    @Test
    public void direcaoLeste(){

        submarino = new Submarino("LDTGDD");
        submarino.setCordenada( new Cordenada(6,5));


        Assert.assertTrue(determinadorDirecao.calcularDirecao(submarino).equals(DirecaoEnum.LESTE.getDirecao()));
    }

    @Test
    public void direcaoOeste(){

        submarino = new Submarino("LDTGDD");
        submarino.setCordenada( new Cordenada(-10,5));


        Assert.assertTrue(determinadorDirecao.calcularDirecao(submarino).equals(DirecaoEnum.OESTE.getDirecao()));
    }
}
