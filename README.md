#Viagem ao fundo do mar

Fiz algumas alterações nas regras para o teste.

Estou considerando que o submarino está andando em um plano cartesiano plano sendo  x para direita e esquerda e y para subir e descer.


Sendo x < 0 esqueda  x > 0 direita.
Sendo y < 0 descendo y > 0 subindo.

Para calcular em qual direção o submarino está indo estou usando o maior valor entre eles
e considerando como destino de norte,sul,leste e oeste

Exemplo:

x= -2 y = 4 indo para o norte

x = -8 y = 6 indo para o leste.

Fiz a implantação do calculo a mais genérica possível, para que tenha flexibilidade
para colocar novas regras de cálculo  com pouca alteração na estrutura do projeto.

Para iniciar o projeto apenas iniciar a classe main SubmarinoApplication.
A configuração da porta é variável então a cada execução será em uma porta diferente, fiz dessa
formas apenas para evitar conflitos.
Fiz um serviço que pode ser acessado da seguinte maneira:

http://localhost:37007/submarino/simples/mover?comando=LMRDDMMU1
